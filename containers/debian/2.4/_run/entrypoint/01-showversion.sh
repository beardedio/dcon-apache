#!/bin/bash
set -euo pipefail

# Output the apache version to the docker log
httpd -v
