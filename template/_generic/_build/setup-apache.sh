#!/bin/bash
set -euo pipefail

# Remove default folders
rm -rf /var/www/localhost
rm -rf /var/www/html

# Create default public folder
mkdir -p /var/www/public

# Crate vhost conf folder to allow for easy config
mkdir /usr/local/apache2/conf/vhosts/

# Overwrite default config of apache
cp /_build/files/apache/httpd.conf /usr/local/apache2/conf/httpd.conf
